import {AfterViewChecked, AfterViewInit, Component, OnInit, ViewChild} from '@angular/core';
import {AdvancedFormComponent} from '@universis/forms';
import {AngularDataContext} from '@themost/angular';
import {ConfigurationService, ErrorService, LoadingService, ModalService} from '@universis/common';
import {TranslateService} from '@ngx-translate/core';
import {ActivatedRoute, Router} from '@angular/router';
import {ToastrService} from 'ngx-toastr';
import {ProfileService} from '../../profile/services/profile.service';
import {HttpClient} from '@angular/common/http';
import {RequestsService} from '../../requests/services/requests.service';

@Component({
  selector: 'app-apply',
  templateUrl: './apply.component.html'
})
export class ApplyComponent implements OnInit {


  // public data: any;
  public department: any;
  public student: any;
  public loading = true;
  public src = 'StudentGradeRemarkActions/new';
  public continueLink = [ '/requests', 'list' ];
  public data = {};
  public studentGradeRemarkAction: any;

  @ViewChild('agree') agree;
  @ViewChild('form') form: AdvancedFormComponent;
  constructor(private _context: AngularDataContext,
              private _errorService: ErrorService,
              private _translateService: TranslateService,
              private _modalService: ModalService,
              private _router: Router,
              private  _toastService: ToastrService,
              private _loadingService: LoadingService,
              private _profileService: ProfileService,
              private _http: HttpClient,
              private _configurationService: ConfigurationService,
              private _activatedRoute: ActivatedRoute,
              private _requestsService: RequestsService ) { }

  showLoading(loading: boolean) {
    this.loading = loading;
    if (loading) {
      this._loadingService.showLoading();
    } else {
      this._loadingService.hideLoading();
    }
  }

  async ngOnInit() {
    this.showLoading(true);

    const student = await this._profileService.getStudent();
    if (student.studentStatus.alternateName !== 'active') {
      this.showLoading(false);
      return this._router.navigate(['/requests/list']);
    }

    const maxNumberOfRemarking = student && student.studyProgram ? student.studyProgram.maxNumberOfRemarking : 0;

    const studentGradeRemarkActions = await this._context.model('StudentRequestActions')
      .asQueryable()
      .where('additionalType').equal('StudentGradeRemarkAction')
      .prepare()
      .and('actionStatus/alternateName').equal('ActiveActionStatus')
      .or('actionStatus/alternateName').equal('CompletedActionStatus')
      .getItems();

    if ((studentGradeRemarkActions && maxNumberOfRemarking) && (studentGradeRemarkActions.length >= maxNumberOfRemarking)) {
      //
      this.showLoading(false);
      return this._router.navigate(['/requests', 'StudentGradeRemarkActions', 'preview']);
    } else {
      Object.assign(this.data,
        {
          academicYear: student.department.currentYear,
          // tslint:disable-next-line:max-line-length
          academicPeriod: student.department.currentPeriod,
          student: student,
          name: this._translateService.instant('StudentGradeRemarkActions.Request')
        });
    }
    this.showLoading(false);
  }

  async onCompletedSubmission($event: any) {
    let continueLink;
    if (this.continueLink && Array.isArray(this.continueLink)) {
      continueLink = this.continueLink.map(x => {
        return x;
      });
    }
    if (continueLink) {
      await this._router.navigate(continueLink);
      this._toastService.show( $event.toastMessage.title, $event.toastMessage.body );
    } else {
      await this._router.navigate(['../'], {relativeTo: this._activatedRoute});
      this._toastService.show( $event.toastMessage.title, $event.toastMessage.body );
    }
  }
}
