import {RouterModule, Routes} from '@angular/router';
import {NgModule} from '@angular/core';
import {ApplyComponent} from './apply/apply.component';
import {PreviewComponent} from './preview/preview.component';

const routes: Routes = [
  {
    path: 'apply',
    component: ApplyComponent
  },
  {
    path: 'preview',
    component: PreviewComponent
  }
];

@NgModule({
  imports: [
    RouterModule.forChild(routes)
  ],
  exports: [RouterModule]
})
export class StudentGradeRemarkActionRoutingModule { }
